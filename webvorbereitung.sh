#!/bin/bash

# dieses Script kann auf Verzeichnisse mit PDF Dokumenten angewendet werden
# damit die Dateinamen webfähig werden und darüber hinaus werden gleich
# Links auf die Dateien erzeugt.


# Setze den Pfad zum Verzeichnis, in dem die Dateien bearbeitet werden sollen
verzeichnis="/home/mpooch/Dokumente/test/testpaket/"

# Pfad zum Webverzeichnis
webpfad="/uploads/0124/test/"

# Erstelle Inhaltsverzeichnisdatei im gleichen Verzeichnis
inhaltVerzeichnisPath="$verzeichnis/Inhaltsverzeichnis.txt"

# Gehe durch jede Datei im Verzeichnis
for datei in "$verzeichnis"/*; do
    # Extrahiere den Dateinamen und die Erweiterung
    dateiname=$(basename -- "$datei")
    erweiterung="${dateiname##*.}"
    dateiname="${dateiname%.*}"

    # Schritte 1 bis 5
    neuerName=$(echo "$dateiname" | sed -e 's/\.pdf$//g' -e 's/\./-/g' -e 's/\ /_/g' -e 's/\,/_/g' -e 's/\ä/ae/g' -e 's/\Ä/Ae/g' -e 's/\ö/oe/g' -e 's/\Ö/Oe/g' -e 's/\ü/ue/g' -e 's/\Ü/Ue/g' -e 's/\ß/ss/g' )

    # Konvertiere den neuen Dateinamen in Kleinbuchstaben
    neuerName=$(echo "$neuerName" | tr '[:upper:]' '[:lower:]')

    # Setze den neuen Dateinamen zusammen und füge die .pdf-Erweiterung hinzu
    neuerDateiname="$neuerName.$erweiterung"

    # Schreibe Eintrag ins Inhaltsverzeichnis
    inhalt="<li><a href=\"$webpfad$neuerDateiname\" target=\"blank\">$dateiname</a></li>"
    echo "$inhalt" >> "$inhaltVerzeichnisPath"

    # Passe den Dateinamen an
    mv "$datei" "$verzeichnis/$neuerDateiname"
done

echo "Bearbeitung abgeschlossen!"
