# Achtung, hier ist es wichtig, dass man diese Datei in Ansi umkonvertiert, weil Windows sonst ein Problem bekommt...
# dieses Powershellscript kann auf Verzeichnisse mit PDF Dokumenten angewendet werden
# damit die Dateinamen webfähig werden und darüber hinaus werden gleich
# Links auf die Dateien erzeugt.


# Setze den Pfad zum Verzeichnis, in dem die Dateien bearbeitet werden sollen
$verzeichnis = "C:\Users\Markus.Pooch\Desktop\Webtool"

# Pfad zum Webverzeichnis
$webpfad="/files/b-plan/januar2024/"

# Erstelle Inhaltsverzeichnisdatei im gleichen Verzeichnis
$inhaltVerzeichnisPath = Join-Path $verzeichnis "Inhaltsverzeichnis.txt"

# Gehe durch jede Datei im Verzeichnis
Get-ChildItem -Path $verzeichnis | ForEach-Object {

    # Extrahiere den Dateinamen und die Erweiterung
    $dateiname = $_.BaseName
    $erweiterung = $_.Extension

    # PDF-Endung und Sonderzeichen ersetzen
    $neuerName = $dateiname -replace '\.pdf$', '' -replace '\.', '-' -replace ' ', '_' -replace ',', '_' -replace 'ä', 'ae' -replace 'Ä', 'Ae' -replace 'ö', 'oe' -replace 'Ö', 'Oe'-replace 'ü', 'ue' -replace 'Ü', 'Ue' -replace 'ß', 'ss'

    # Konvertiere den neuen Dateinamen in Kleinbuchstaben
    $neuerName = $neuerName.ToLower()

    # Setze den neuen Dateinamen zusammen und füge die .pdf-Erweiterung hinzu
    $neuerDateiname = $neuerName + $erweiterung

    # Schreibe Eintrag ins Inhaltsverzeichnis
    $inhalt = "<li><a href=`"$webpfad$neuerDateiname`" target=`"blank`">$dateiname</a></li>"
    Add-Content -Path $inhaltVerzeichnisPath -Value $inhalt

    # Passe den Dateinamen an
    Rename-Item $_.FullName -NewName $neuerDateiname

}

Write-Host "Bearbeitung abgeschlossen!"
